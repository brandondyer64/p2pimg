const p2pimgversion = 1

const crypto = require('crypto')
const Swarm = require('discovery-swarm')
const defaults = require('dat-swarm-defaults')
const getPort = require('get-port')
const readline = require('readline')
const Jimp = require('jimp')

const config = require('./config.json')

/**
 * Here we will save our TCP peer connections
 * using the peer id as key: { peer_id: TCP_Connection }
 */
const peers = {}
// Counter for connections, used for identify connections
let connSeq = 0

const shreds = {}

const imgconfigs = {}

var waitForConfig = false
var waitForShreds = []
var saveTo = null
var recievedShreds = {}
const shredToPeers = {}

// Peer Identity, a random hash for identify your peer
const myId = crypto.randomBytes(32)
console.log('Your identity: ' + myId.toString('hex'))

function hash(data, length = 16) {
  const hasher = crypto.createHash('sha256')
  hasher.update(data)
  return hasher.digest('hex').substr(0, length)
}

// reference to redline interface
let rl
/**
 * Function for safely call console.log with readline interface active
 */
function log() {
  if (rl) {
    rl.clearLine()
    rl.close()
    rl = undefined
  }
  for (let i = 0, len = arguments.length; i < len; i++) {
    console.log(arguments[i])
  }
  askUser()
}

/*
* Function to get text input from user and send it to other peers
* Like a chat :)
*/
const askUser = async () => {
  rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
  })

  rl.question('~$ ', message => {
    let params = message.split(' ')
    let command = params.shift()
    switch (command) {
      case 'shredcount':
        console.log(Object.keys(shreds).length)
        break
      case 'shredlist':
        console.log(JSON.stringify(shreds, true, 2))
        break
      case 'imagecount':
        console.log(Object.keys(imgconfigs).length)
        break
      case 'imagelist':
        for (let i in imgconfigs) {
          console.log(i)
        }
        break
      case 'push':
        // Load image
        Jimp.read(params.join(' '))
          .then(image => {
            console.log()
            // Resize and grayscale
            image.resize(config.imagesize, config.imagesize).greyscale()
            // Shred
            let imgconfig = ''
            for (let y = 0; y < config.imagesize; y++) {
              let shred = ''
              let subshred = ''
              for (let x = 0; x < config.imagesize; x++) {
                let index = image.getPixelIndex(x, y)
                let color = Math.floor(image.bitmap.data[index] / 16)
                let chr = color.toString(16)
                shred += chr
                if (x % 5 == 0) {
                  subshred += chr
                }
              }
              if (y % 10 == 0) {
                console.log(subshred)
              }
              // Broadcast to peers
              for (let id in peers) {
                peers[id].conn.write('|ps:' + shred)
              }
              let shredHash = hash(shred)
              imgconfig += '$' + shredHash
              shreds[shredHash] = shred
            }
            imgconfigs[hash(imgconfig, 32)] = imgconfig
            console.log(imgconfig)
            console.log(hash(imgconfig, 32))
            // Broadcast config
            for (let id in peers) {
              peers[id].conn.write('|pc:' + imgconfig)
            }
          })
          .catch(err => {
            console.log(err)
          })
        break
      case 'get':
        waitForConfig = params.shift()
        for (let id in peers) {
          peers[id].conn.write('|gc:' + waitForConfig)
        }
        console.log('sent request')
        break
      case '':
        break
      default:
        console.log(`${command} is not a command`)
    }
    rl.close()
    rl = undefined
    askUser()
  })
}

/**
 * Default DNS and DHT servers
 * This servers are used for peer discovery and establishing connection
 */
const swarmConfig = defaults({
  // peer-id
  id: myId
})

/**
 * discovery-swarm library establishes a TCP p2p connection and uses
 * discovery-channel library for peer discovery
 */
const sw = Swarm(swarmConfig)
;(async () => {
  // Choose a random unused port for listening TCP peer connections
  const port = await getPort()

  sw.listen(port)
  console.log('Listening to port: ' + port)

  /**
   * The channel we are connecting to.
   * Peers should discover other peers in this channel
   */
  const channelName =
    `p2pimg` +
    `-${config.imagesize}` +
    `-${config.color ? 'c' : 'g'}` +
    `-v${p2pimgversion}` +
    `-c${config.channel}`
  sw.join(channelName)
  console.log(channelName)

  sw.on('connection', (conn, info) => {
    // Max peers
    if (peers.length >= config.maxpeers) return
    if (peers.length >= config.maxpeers / 2 && Math.random() >= 0.5) return

    // Connection id
    const seq = connSeq

    const peerId = info.id.toString('hex')
    log(`Connected #${seq} to peer: ${peerId}`)

    // Keep alive TCP connection with peer
    if (info.initiator) {
      try {
        conn.setKeepAlive(true, 600)
      } catch (exception) {
        log('exception', exception)
      }
    }

    conn.on('data', data => {
      // Here we handle incomming messages
      // console.log(peerId + ' ' + data.toString())
      const datastring = data.toString()
      if (datastring.startsWith('|')) {
        datas = datastring.split('|')
        for (let i in datas) {
          let message = datas[i]
          if (message == '') continue
          let params = message.split(':')
          // vars
          let shred = null
          let shredHash = null
          let configHash = null
          let imgconfig = null
          switch (params.shift()) {
            case 'ps':
              shred = params[0]
              shredHash = hash(shred)
              if (Math.random() < 0.1) {
                shreds[shredHash] = shred
              } else if (!(shredHash in shreds)) {
                for (let id in peers) {
                  peers[id].conn.write('|ps:' + shred)
                }
              }
              if (waitForShreds.includes(shredHash)) {
                recievedShreds[shredHash] = shred
              }
              if (shredHash in shredToPeers) {
                peers[shredToPeers[shredHash]].conn.write('|ps:' + shred)
                delete shredToPeers[shredHash]
              }
              break
            case 'gs':
              shredHash = params[0]
              if (shredHash in shreds) {
                conn.write('|ps:' + shreds[shredHash])
              } else {
                if (Math.random() < 0.5) {
                  shredToPeers[shredHash] = peerId
                  for (let id in peers) {
                    peers[id].conn.write('|gs:' + shredHash)
                  }
                }
              }
              break
            case 'pc':
              imgconfig = params[0]
              configHash = hash(imgconfig, 32)
              console.log(configHash)
              if (Math.random() < 0.1) {
                imgconfigs[configHash] = imgconfig
              } else if (!(hash(imgconfig) in imgconfigs)) {
                for (let id in peers) {
                  peers[id].conn.write('|pc:' + imgconfig)
                }
              }
              if (configHash == waitForConfig) {
                console.log('Config recieved.')
                waitForConfig = false
                shredHashes = imgconfig.split('$')
                shredHashes.shift()
                for (let i in shredHashes) {
                  let shredHash = shredHashes[i]
                  for (let id in peers) {
                    peers[id].conn.write('|gs:' + shredHash)
                  }
                }
                setTimeout(() => {
                  console.log('Recieved:')
                  for (let i in recievedShreds) {
                    console.log(i)
                  }
                }, config.imagewaittime * 1000)
              }
              break
            case 'gc':
              configHash = params[0]
              console.log(configHash)
              if (configHash in imgconfigs) {
                conn.write('|pc:' + shreds[shredHash])
              } else {
                if (Math.random() < 0.5) {
                  configToPeers[configHash] = peerId
                  for (let id in peers) {
                    peers[id].conn.write('|gc:' + shredHash)
                  }
                }
              }
              break
          }
        }
      }
    })

    conn.on('close', () => {
      // Here we handle peer disconnection
      // log(`Connection ${seq} closed, peer id: ${peerId}`)
      // If the closing connection is the last connection with the peer, removes the peer
      if (peers[peerId] && peers[peerId].seq === seq) {
        delete peers[peerId]
      }
    })

    // Save the connection
    if (!peers[peerId]) {
      peers[peerId] = {}
    }
    peers[peerId].conn = conn
    peers[peerId].seq = seq
    connSeq++
  })

  // Read user message from command line
  askUser()
})()
